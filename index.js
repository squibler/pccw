/**
 * Example problem with existing solution and passing test.
 * See problem 0 in the spec file for the assertion
 * @returns {string}
 */
exports.example = () => 'hello world';

/**
 * Problem 1 solution:
 *
 * 1. iterate through the provided collection using .map()
 * 2. filter out the unwanted private keys of each item
 * 3. use the resulting array of acceptable keys to setup a clean return object
 * 4. return the clean object
 */
exports.stripPrivateProperties = (privateProperties, collection) => collection.map((item) => {
  return Object.keys(item)
    .filter(key => !privateProperties.includes(key))
    .reduce((object, key) => ({ ...object, [key]: item[key] }), {});
});

/**
 * Problem 2 solution:
 *
 * 1. iterate through the provided collection
 * 3. confirm the collection item does not contain the provided exclusionProperty
 * 4. push the confirmed item onto the results
 * 5. return the results
 */
exports.excludeByProperty = (exclusionProperty, collection) => {
  const result = [];
  collection.forEach((item) => {
    if (!hasOwnProperty.call(item, exclusionProperty)) {
      result.push(item);
    }
  });
  return result;
};

/**
 * Problem 3 solution:
 *
 * 1. iterate through the provided collection with defined structure
 * 2. extract the object values
 * 3. calculate the sum
 * 4. return the calculated result in the required format
 */
exports.sumDeep = collection => collection.map(({ objects }) => Object.values(objects)
  .reduce((object, current) => {
    const result = object;
    result.objects += current.val;
    return result;
  }, { objects: 0 }));

/**
 * Problem 4 solution:
 *
 * I originally started to use map once again but changed once
 * I realised there was an error that did not have an assigned
 * colour.
 *
 * 1. iterate over the provided collection
 * 2. extract the colour key from the provided colours
 *    based on the status code of the item
 * 3. confirm colour exists for status
 * 4. commit to result array
 * 5. return results
 */
exports.applyStatusColor = (colors, collection) => {
  const result = [];
  collection.forEach((item) => {
    const color = Object.keys(colors).find(key => colors[key].find(val => val === item.status));
    // The red herring, only add to results if there is a color, 408 error has none
    if (color) {
      result.push({ status: item.status, color });
    }
  });
  return result;
};

/**
 * Problem 5 solution:
 *
 * 1. Pass provided closure and greeting to return method
 * 2. accept name as parameter of returned method
 * 3. when method is called back, return the result of the provided closure
 */
exports.createGreeting = (closure, greeting) => name => closure(greeting, name);

/**
 * Problem 6 solution
 *
 * 1. accept defaults settings on inital call
 * 2. return callback method for assigning userSettings
 * 3. merge defaults with userSettings
 * 4. return merged settings
 */
exports.setDefaults = defaults => (userSettings) => {
  const settings = { ...defaults, ...userSettings };
  return settings;
};

/**
 * Problem 7 solution:
 *
 * 1. create an async method to await fetching of user
 *    data from proviided services
 * 2. return user data from the async method
 * 3. Return a promise which resolves the user data retrieved
 *    via the fetUserDataAsync function
 */
async function fetchUserDataAsync(userName, services) {
  const status = await services.fetchStatus();
  const users = await services.fetchUsers();
  const user = users.find(item => item.name === userName);
  const company = await services.fetchCompanyById(user.companyId);
  return { status, user, company };
}
exports.fetchUserByNameAndUsersCompany = (userName, services) => new Promise((resolve) => {
  resolve(fetchUserDataAsync(userName, services));
});
